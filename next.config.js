/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    remotePatterns: [
      {
        hostname: "rickandmorty.com"
      },
    ],
  }
}

module.exports = nextConfig
