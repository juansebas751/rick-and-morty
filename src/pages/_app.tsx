import type { ReactElement, ReactNode } from 'react'
import '../styles/globals.css'
import { NextPage } from 'next'
import type { AppProps } from 'next/app'
import { Provider } from 'react-redux'
import { store } from '../store/store'
import { AppProvider } from '@/context/AppProvider'
export type NextPageWithLayout<P = {}, IP = P> = NextPage<P, IP> & {
  getLayout?: (page: ReactElement) => ReactNode
}
 
type AppPropsWithLayout = AppProps & {
  Component: NextPageWithLayout
}
 
export default function MyApp({ Component, pageProps }: AppPropsWithLayout) {
  
  const getLayout = Component.getLayout ?? ((page) => page)
 
  return (
    <AppProvider>
      <Provider store={store}>
        { getLayout(<Component {...pageProps} />) }
      </Provider>
    </AppProvider>
  );
}
