import SearchBox from '@/components/searchBox';

export default function Home() {
  return (
        <div className="main"> 
          <SearchBox />
        </div>
  )
}
