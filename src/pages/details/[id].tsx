import CardDetails from "@/components/cardDetails";
import Layout from "@/components/layout";
import Loading from "@/components/loading";
import { useGetDataByIdQuery } from "@/store/api/rickAndMortyApi";
import { useRouter } from "next/router";


export default function oneCharacter() {

    const router = useRouter();
    const { id } = router.query;

    const { data, isLoading } = useGetDataByIdQuery({ id: id as string })

    return (
        <Layout title={`${data?.name}`}>
            {
                isLoading || !data ? (  
                        <Loading />
                    ) : (
                        <div className="detail flex items-center justify-center pt-20">
                            <CardDetails data={data} />
                        </div>
            )}
        </Layout>
    )
}