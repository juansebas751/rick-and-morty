import Card from "@/components/card";
import Layout from "@/components/layout";
import { useAppContext } from "@/context/AppContext";

export default function Favorites() {

    const { appState } = useAppContext();
    const { favorites } = appState;

    return (
        <Layout>
        <div className="container py-10">
            <h1 className="mb-10 text-3xl text-center sm:text-start font-extrabold">Favorites</h1>
            <div className="grid gap-6 grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
                {
                    favorites.map((character) => (
                        <Card key={character.id} data={character} />
                ))}
            </div>
        </div>
        </Layout>
    )
}