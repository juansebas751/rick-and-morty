import { createContext, useContext } from "react";
import { Character, IAppContext } from "@/interfaces";

export type AppContextProps = {
    appState: IAppContext,
    addToFavorites: (character: Character) => void;
    removeFromFavorites: (characterId: number) => void;
    setCurrentPage: (page: number) => void;
    setSearchTerm: (term: string) => void;
}

export const AppContext = createContext<AppContextProps>({} as AppContextProps);

export const useAppContext = () => {
    const context = useContext(AppContext)
    if(!context) {
        throw new Error("useAppContext must be use within an AppProvider")
    }
    return context;
}
