import { Character, IAppContext } from "@/interfaces";

type AppAction =
    | { type: "addToFavorites"; payload: Character }
    | { type: "removeFromfavorites"; payload: number }
    | { type: "setSearchTerm"; payload: string }
    | { type: "setCurrentPage"; payload: number }


export const appReducer = ( state: IAppContext, action: AppAction ) => {

    switch (action.type) {
        case "addToFavorites":
          return { ...state, favorites: [...state.favorites, action.payload ] };
        case "removeFromfavorites":
          return { ...state, favorites: state.favorites.filter((fav) => fav.id !== action.payload) };
        case "setSearchTerm":
          return { ...state, searchTerm: action.payload, currentPage: 1 };
        case "setCurrentPage":
          return { ...state, currentPage: action.payload };
        default:
          return state;
      }
}