import { Character, IAppContext, IAppProvider } from "@/interfaces";
import { AppContext } from "./AppContext";
import { useReducer } from "react"
import { appReducer } from "./appReducer";

const initialState: IAppContext = {
    currentPage: 1,
    searchTerm: "",
    favorites: []
}

export const AppProvider = ({ children }: IAppProvider) => {

    const [ appState, dispatch ] = useReducer(appReducer, initialState);

    const addToFavorites = (character: Character) => {
        dispatch({ type: "addToFavorites", payload: character })
    }

    const removeFromFavorites = (characterId: number) => {
        dispatch({ type: "removeFromfavorites", payload: characterId })
    }

    const setCurrentPage = (page: number) => {
        dispatch({ type: "setCurrentPage", payload: page })
    }

    const setSearchTerm = (term: string) => {
        dispatch({ type: "setSearchTerm", payload: term })
    }

    return (
        <AppContext.Provider value={{
            appState,
            addToFavorites,
            removeFromFavorites,
            setCurrentPage,
            setSearchTerm
        }}>
            { children }
        </AppContext.Provider>
    )
}