export interface Character {
    id: number;
    name: string;
    status: string;
    species: string;
    gender: string;
    image: string;
    origin: {
        name: string
    }
    location: {
        name: string
    }
}

export interface IPagination {
    currentPage: number;
    setCurrentPage: React.Dispatch<React.SetStateAction<number>>;
}

export interface IPageState {
    page: number;
    favorites: [];
}

export interface Ilayout {
    children: React.ReactNode,
    title?: string
}

export interface ICard {
    data: Character
}

export interface IAppProvider {
    children: JSX.Element | JSX.Element[]
}

export interface IAppContext {
    currentPage: number;
    favorites: Character[];
    searchTerm: string;
}