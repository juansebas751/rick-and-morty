import Image from "next/image";
import logo  from "../assets/rickAndMorty.png";
import Link from "next/link";
import { MdFavoriteBorder } from "react-icons/md";

export default function Header() {

  return (
    <div className="header flex items-center justify-between bg-[#3C3E44]">
      <Link className="flex items-center justify-center" href="/">
        <Image src={logo} className="logo w-32" alt="Rick and Morty"/> {/* ml-15 w-20 */}
        <h1 className="font-extrabold text-2xl sm:text-3xl">Rick and Morty</h1>
      </Link>
      <Link className="font-extrabold w-24 text-4xl flex justify-center sm:block" href="/favorites">
        <MdFavoriteBorder />
      </Link>
    </div>
  )
}
