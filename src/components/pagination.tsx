import { useAppContext } from "@/context/AppContext";
import { useGetDataByNameQuery } from "@/store/api/rickAndMortyApi";
import { PiArrowCircleLeftBold, PiArrowCircleRightBold } from "react-icons/pi";
import { TbCircleArrowLeftFilled, TbCircleArrowRightFilled } from "react-icons/tb";
import Loading from "./loading";

export default function Pagination() {

    const { setCurrentPage, appState } = useAppContext();
    const { currentPage, searchTerm } = appState

    const { data, isLoading, isFetching } = useGetDataByNameQuery({ name: searchTerm, page: currentPage })
    const totalPages = data?.info.pages || 1;

    const nextPage = () => {
        setCurrentPage(currentPage + 1)
    }

    const prevPage = () => {
        setCurrentPage(currentPage - 1)
    }

    if(isLoading || isFetching) return <Loading />

    return (
            <div className="container h-24 flex items-center justify-around relative">
                { 
                    currentPage > 2 ? (
                        <button className="font-bold hidden sm:flex items-center rounded-lg absolute text-3xl left-5 text-[#9E9E9E]" onClick={() => setCurrentPage(1)}>
                            <TbCircleArrowLeftFilled />
                            <span className="text-base ml-2">Page 1</span>
                        </button>
                    ) : null
                }
                <button className="font-bold hidden sm:flex items-center rounded-lg absolute text-3xl right-5 text-[#9E9E9E]" onClick={() => setCurrentPage(totalPages)}>
                    <span className="text-base mr-2">Page {totalPages}</span>
                    <TbCircleArrowRightFilled />
                </button>
                <div className="font-bold container w-80 flex items-center justify-around">
                    <button className="text-3xl" onClick={prevPage} disabled={currentPage === 1}>
                        <PiArrowCircleLeftBold />
                    </button>
                    <span>Page {currentPage}</span>
                    <button className="text-3xl" onClick={nextPage} disabled={currentPage === totalPages}>
                        <PiArrowCircleRightBold />
                    </button>
                </div>
            </div>
    )
}