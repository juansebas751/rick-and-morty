import { useGetDataByNameQuery } from "@/store/api/rickAndMortyApi"
import Card from "./card";
import { FaSearch } from "react-icons/fa";
import Pagination from "./pagination";
import { useAppContext } from "@/context/AppContext";
import Layout from "./layout";

export default function SearchBox() {

    const { setCurrentPage, setSearchTerm, appState } = useAppContext();
    const { searchTerm, currentPage } = appState; 

    const { data, isLoading } = useGetDataByNameQuery({ name: searchTerm, page: currentPage });

    const characters = data?.results ?? [];

    const handleSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
      e.preventDefault();
      setSearchTerm(e.target.value);
      setCurrentPage(1);
    }

    return (
        <Layout>
        <div className="flex items-center justify-center mt-10">
            <form className="relative">
            <input 
            className="text-gray-500 w-72 p-3 rounded-md h-8 outline-none"
            type="text"
            value={searchTerm}
            placeholder="Search for name"
            onChange={handleSearch}
             />
            <button 
            className="h-8 bg-white text-gray-500 right-2 absolute"
            disabled={isLoading}
            type="submit">
                <FaSearch />
            </button>
            
        </form>
        </div>
        <Pagination />
        <div>      
            <div className="container grid gap-6 grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4"> {/** w-10/12 */}
            {
              characters.map((character) => (
                <Card key={character.id} data={character} />
            ))}
            </div>
      </div>
      <Pagination />
        </Layout>
    )
}