import Image from "next/image";
import portal from "../assets/portal.png"

export default function Loading() {
  return (
    <>
      <div className="loading flex items-center justify-center flex-col">
       <div>
        <Image width="400" height="400" src={portal} alt="portal" />
          <p className="text-center font-bold text-2xl bottom-10">Loading...</p>
       </div>
      </div>
    </>
  )
}
