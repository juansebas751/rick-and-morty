import { useAppContext } from "@/context/AppContext";
import { ICard } from "@/interfaces";
import Link from "next/link";  
import { MdFavoriteBorder, MdFavorite  } from "react-icons/md";

type Status = string | undefined;

export default function Card({ data }: ICard) {

    const { addToFavorites, removeFromFavorites, appState } = useAppContext()
    const { favorites } = appState;

    const isFavorite = favorites.some((fav) => fav.id  === data.id)

    const handleFavorite = (e: React.MouseEvent<HTMLButtonElement>) => {
        e.preventDefault();

        if (isFavorite) {
            removeFromFavorites(data.id)
        } else {
            addToFavorites(data)
        }
    }

    const validateColor = (type: string): Status => {
        if (type === "Alive") {
            return "green"
        } else if (type === "Dead") {
            return "red"
        } else {
            return "gray"
        }
    }

    const validateWidth = (state: string): Status => {
        if (state === "unknown") {
            return "7rem"
        }
    }

    return (   
            <Link href={`/details/${data.id}`} key={data.id} className="container rounded-lg w-72 bg-[#3C3E44] relative">
                <div className="status flex items-center absolute bg-slate-600 top-2 w-24" style={{ width: validateWidth(data.status) }}>
                    <div className="rounded-lg w-4 h-4 ml-2" style={{ background: validateColor(data.status) }}></div>
                    <p className="my-2 ml-2">{data.status}</p>
                </div>
                    <img className="w-72 rounded-lg" src={data.image} alt={data.name} />
                <h3 className="my-2 text-center font-semibold text-xl">{data.name}</h3>
                <hr className="w-56"/>
                <div className="p-5 pt-2">
                    <div className="flex justify-between">
                        <div className="capitalize">
                            <span className="text-sm font-semibold">Species:</span>
                            <p>{data.species}</p>
                            <span className="text-sm font-semibold">Origin:</span>
                            <p>{data.origin.name}</p>
                        </div>
                        <div className="mr-2 mt-2 text-3xl">
                            <button onClick={handleFavorite}>
                                {
                                    !isFavorite ? <MdFavoriteBorder /> : <MdFavorite />
                                }
                            </button>
                        </div>
                    </div>
                </div>
            </Link>
    )
}

