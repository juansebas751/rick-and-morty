import Head from 'next/head'
import Footer from './footer'
import Header from './header'
import { Ilayout } from '@/interfaces'

export default function Layout({ children, title }: Ilayout) {
  return (
    <>
      <Head>
        <title>{title}</title>
        <link rel="shortcut icon" href="" type="image/x-icon" />
      </Head>
      <Header />
      <main className="min-h-screen flex flex-col">
        {children}
      </main>
      <Footer />
    </>
  )
}

Layout.defaultProps = {
  title: "Rick & Morty"
}
