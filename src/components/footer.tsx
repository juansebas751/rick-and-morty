import { FaGithub, FaLinkedin  } from "react-icons/fa";
import { FaGitlab } from "react-icons/fa6";

export default function Footer() {
    return (
        <div className="footer h-28 font-semibold w-full bg-[#3C3E44] flex justify-center items-center">
           <div className="flex items-center justify-center flex-col">
                <h2 className="text-xl">Juan Herrera</h2>
                <div className="flex justify-between m-2 text-2xl w-28">
                    <a href="https://github.com/1juanherrera" target="_blank" rel="noopener noreferrer">
                        <FaGithub  />
                        </a>
                    <a href="https://www.linkedin.com/in/juanherreramu%C3%B1oz/" target="_blank" rel="noopener noreferrer">
                        <FaLinkedin  />
                        </a>
                    <a href="https://gitlab.com/juansebas751" target="_blank" rel="noopener noreferrer">
                        <FaGitlab  />   
                        </a>
                </div>
           </div>
        </div>
    )
}