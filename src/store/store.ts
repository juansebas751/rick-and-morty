import { configureStore } from "@reduxjs/toolkit";
import { rickAndMortyApi } from "./api/rickAndMortyApi";
import { setupListeners } from "@reduxjs/toolkit/query";

export const store = configureStore({
    reducer: {
        "rickAndMortyApi": rickAndMortyApi.reducer
    },
    middleware: (getDefaultMiddleware) => 
        getDefaultMiddleware().concat(rickAndMortyApi.middleware)
})

setupListeners(store.dispatch);

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

