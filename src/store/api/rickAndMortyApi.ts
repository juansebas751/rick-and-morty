import { Character } from "@/interfaces";
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const rickAndMortyApi = createApi({
    reducerPath: "rickAndMortyApi",
    baseQuery: fetchBaseQuery({
        baseUrl: "https://rickandmortyapi.com/api"
    }),
    endpoints: (builder) => ({
        getData: builder.query<{ results: Character[]; info: { pages: number } }, { page?: number }>({
            query: ({ page = 1}) => `character/?page=${page}`
        }),
        getDataById: builder.query<Character, {id: string}>({
            query: ({ id }) => `character/${id}`
        }),
        getDataByName: builder.query<{ results: Character[]; info: { pages: number } }, { name?: string; page?: number }>({
            query: ({ name, page = 1 }) => `character/?name=${name || ''}&page=${page}`,
        }),
    })
})

export const { useGetDataQuery, useGetDataByIdQuery, useGetDataByNameQuery } = rickAndMortyApi